#!/usr/bin/make -f

# CMake doesn't use CPPFLAGS, pass them to CFLAGS/CXXFLAGS to enable the
# missing (hardening) flags.
export DEB_CFLAGS_MAINT_APPEND   = $(shell dpkg-buildflags --get CPPFLAGS)
export DEB_CXXFLAGS_MAINT_APPEND = $(shell dpkg-buildflags --get CPPFLAGS)

%:
	dh $@ --with python2 --with python3 --buildsystem=cmake --parallel

# Python versions
PYVERSIONS=$(shell pyversions -v -r)
DEFAULT_PY=$(shell pyversions -v -d)
PY3VERSIONS=$(shell py3versions -v -r)
DEFAULT_PY3=$(shell py3versions -v -d)
LIBPYSIDE_SOVERSION=1.2

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

# the python version we are being called on atm, in the form "34" (for 3.4)
PYVER=$(subst .,,$*)
# python ≥ 3.5 append the multiarch triplet to module names (in particular this
# is reflected in ShibokenConfig.cmake)
PYMODULESUFFIX=$(if $(shell test $(PYVER) -ge 35 && echo "yes"),"-$(DEB_HOST_MULTIARCH)","")

ifneq (,$(filter debug,$(DEB_BUILD_OPTIONS)))
	BD_DEBUG           = python-all-dbg (>= 2.6.6-3~), python3-all-dbg, shiboken-dbg (>= 1.2.2),
endif

ifneq (,$(filter python-pyside-dbg,$(shell dh_listpackages)))
	BUILD_DEBUG = true
	CONFIGURE2_TARGETS = $(PYVERSIONS:%=override_dh_auto_configure_dbg-%)  $(PYVERSIONS:%=override_dh_auto_configure-%)
	BUILD2_TARGETS     = $(PYVERSIONS:%=override_dh_auto_build_dbg-%)  $(PYVERSIONS:%=override_dh_auto_build-%)
	INSTALL2_TARGETS   = $(PYVERSIONS:%=override_dh_auto_install_dbg-%)    $(PYVERSIONS:%=override_dh_auto_install-%)
	TEST2_TARGETS      = $(PYVERSIONS:%=override_dh_auto_test_dbg-%)  $(PYVERSIONS:%=override_dh_auto_test-%)
	
	CONFIGURE3_TARGETS = $(PY3VERSIONS:%=override_dh_auto_configure_dbg_3-%) $(PY3VERSIONS:%=override_dh_auto_configure_3-%)
	BUILD3_TARGETS     = $(PY3VERSIONS:%=override_dh_auto_build_dbg-%) $(PY3VERSIONS:%=override_dh_auto_build-%)
	INSTALL3_TARGETS   = $(PY3VERSIONS:%=override_dh_auto_install_3_dbg-%) $(PY3VERSIONS:%=override_dh_auto_install_3-%)
	TEST3_TARGETS      = $(PY3VERSIONS:%=override_dh_auto_test_dbg-%) $(PY3VERSIONS:%=override_dh_auto_test-%)
else
	CONFIGURE2_TARGETS = $(PYVERSIONS:%=override_dh_auto_configure-%)
	BUILD2_TARGETS     = $(PYVERSIONS:%=override_dh_auto_build-%)
	INSTALL2_TARGETS   = $(PYVERSIONS:%=override_dh_auto_install-%)
	TEST2_TARGETS      = $(PYVERSIONS:%=override_dh_auto_test-%)
	
	CONFIGURE3_TARGETS = $(PY3VERSIONS:%=override_dh_auto_configure_3-%)
	BUILD3_TARGETS     = $(PY3VERSIONS:%=override_dh_auto_build-%)
	INSTALL3_TARGETS   = $(PY3VERSIONS:%=override_dh_auto_install_3-%)
	TEST3_TARGETS      = $(PY3VERSIONS:%=override_dh_auto_test-%)
endif

update-control:
	sed -e 's/@BD-DEBUG@/$(BD_DEBUG)/g' debian/control.in > debian/control
ifdef BD_DEBUG
	cat debian/control.dbg-packages >> debian/control
endif

# G++ versions
g++-version := $(shell readlink /usr/bin/g++ | sed -e 's/g++-\(.*\)\.\(.*\)/\1\2/')
g++-version-ge-4.5 := $(shell if test $(g++-version) -ge 45; then echo "yes"; else echo "no"; fi)

CMAKE_COMMON_OPTIONS := -DCMAKE_SKIP_RPATH=true \
			-DLIB_SUFFIX=/$(DEB_HOST_MULTIARCH) \
			-DUSE_XVFB=true \
			-DCTEST_TESTING_TIMEOUT=600

#### CONFIGURE ####

override_dh_auto_configure: configure_2 configure_3

configure_2: $(CONFIGURE2_TARGETS)
	@echo "Python2 versions configured."

configure_3: $(CONFIGURE3_TARGETS)
	@echo "Python3 versions configured."

override_dh_auto_configure_dbg-%:
	mkdir -p build-$*-dbg
	dh_auto_configure -a --builddirectory=build-$*-dbg -- \
	$(CMAKE_COMMON_OPTIONS) \
	-DCMAKE_BUILD_TYPE:STRING="Debug" \
	-DPYTHON_SUFFIX=-python$*-dbg \

override_dh_auto_configure-%:
	mkdir -p build-$*
	dh_auto_configure -a --builddirectory=build-$* -- \
	$(CMAKE_COMMON_OPTIONS) \
	-DCMAKE_BUILD_TYPE:STRING="Release" \
	-DPYTHON_SUFFIX=-python$* \
	-DDEFAULT_PYTHON_BASENAME=-python${DEFAULT_PY}

override_dh_auto_configure_dbg_3-%:
	mkdir -p build-$*-dbg
	dh_auto_configure -a --builddirectory=build-$*-dbg -- \
	$(CMAKE_COMMON_OPTIONS) \
	-DCMAKE_BUILD_TYPE:STRING="Debug" \
	-DPYTHON_SUFFIX=.cpython-$(PYVER)dm$(PYMODULESUFFIX)

override_dh_auto_configure_3-%:
	mkdir -p build-$*
	dh_auto_configure -a --builddirectory=build-$* -- \
	$(CMAKE_COMMON_OPTIONS) \
	-DCMAKE_BUILD_TYPE:STRING="Release" \
	-DPYTHON_SUFFIX=.cpython-$(PYVER)m$(PYMODULESUFFIX) \
	-DDEFAULT_PYTHON_BASENAME=-python${DEFAULT_PY}

#### BUILD ####

override_dh_auto_build: build_2 build_3

build_2: $(BUILD2_TARGETS)
	@echo "Python2 versions built."

build_3: $(BUILD3_TARGETS)
	@echo "Python3 versions built."

override_dh_auto_build_dbg-%:
	dh_auto_build -a --builddirectory=build-$*-dbg

override_dh_auto_build-%:
	python setup/setup.py egg_info
	dh_auto_build -a --builddirectory=build-$*

#### INSTALL ####

override_dh_auto_install: install_2 install_3

install_2: $(INSTALL2_TARGETS)
	@echo "Python2 versions installed."

install_3: $(INSTALL3_TARGETS)
	@echo "Python3 versions installed."

override_dh_auto_install_dbg-%:
	dh_auto_install -a --builddirectory=build-$*-dbg --destdir=debian/tmp-dbg
	# Name the debug .so's correctly for python2
	rename 's|(.*)\.so(.*)$$|$$1_d.so$$2|'\
		debian/tmp-dbg/usr/lib/python2*/*-packages/PySide/*.so

override_dh_auto_install-%:
	dh_auto_install -a --builddirectory=build-$*

override_dh_auto_install_3_dbg-%:
	dh_auto_install -a --builddirectory=build-$*-dbg --destdir=debian/tmp-dbg
	# Name the debug .so's correctly for python3
	rename 's|(.*)/PySide/([^\.]*)\.so$$|$$1/PySide/$$2.cpython-$(subst .,,$*)dm.so|'\
		debian/tmp-dbg/usr/lib/python3/*-packages/PySide/*.so

override_dh_auto_install_3-%:
	dh_auto_install -a --builddirectory=build-$*
	# Name the .so's correctly for python3
	rename 's|(.*)/PySide/([^\.]*)\.so$$|$$1/PySide/$$2.cpython-$(subst .,,$*)m.so|'\
		debian/tmp/usr/lib/python3/*-packages/PySide/*.so

#### Shared libs ####
override_dh_makeshlibs: $(PYVERSIONS:%=override_dh_makeshlibs-%) $(PY3VERSIONS:%=override_dh_makeshlibs_py3-%)
	cat debian/libpyside${LIBPYSIDE_SOVERSION}.symbols-py* > debian/libpyside${LIBPYSIDE_SOVERSION}.symbols
	cat debian/libpyside-py3-${LIBPYSIDE_SOVERSION}.symbols-py* > debian/libpyside-py3-${LIBPYSIDE_SOVERSION}.symbols
	dh_makeshlibs -a

override_dh_makeshlibs-%:
	echo "libpyside-python$*.so.${LIBPYSIDE_SOVERSION} libpyside${LIBPYSIDE_SOVERSION} #MINVER#" > debian/libpyside${LIBPYSIDE_SOVERSION}.symbols-py$*
	cat debian/libpyside${LIBPYSIDE_SOVERSION}.symbols.pristine >> debian/libpyside${LIBPYSIDE_SOVERSION}.symbols-py$*
ifeq ($(g++-version-ge-4.5),yes)
	# g++ version is >= 4.5, add related symbols
	sed -e 's|^ _| (ge-g++4.5)_|g' debian/libpyside${LIBPYSIDE_SOVERSION}.symbols.g++-ge-4.5 >> debian/libpyside${LIBPYSIDE_SOVERSION}.symbols-py$*
else
	# g++ version is < 4.5, add related symbols
	sed -e 's|^ _| (lt-g++4.5)_|g' debian/libpyside${LIBPYSIDE_SOVERSION}.symbols.g++-lt-4.5 >> debian/libpyside${LIBPYSIDE_SOVERSION}.symbols-py$*
endif

override_dh_makeshlibs_py3-%:
	echo "libpyside.cpython-$(subst .,,$*)m.so.${LIBPYSIDE_SOVERSION} libpyside-py3-${LIBPYSIDE_SOVERSION} #MINVER#" > debian/libpyside-py3-${LIBPYSIDE_SOVERSION}.symbols-py$*
	cat debian/libpyside${LIBPYSIDE_SOVERSION}.symbols.pristine >> debian/libpyside-py3-${LIBPYSIDE_SOVERSION}.symbols-py$*
ifeq ($(g++-version-ge-4.5),yes)
	# g++ version is >= 4.5, add related symbols
	sed -e 's|^ _| (ge-g++4.5)_|g' debian/libpyside${LIBPYSIDE_SOVERSION}.symbols.g++-ge-4.5 >> debian/libpyside-py3-${LIBPYSIDE_SOVERSION}.symbols-py$*
else
	# g++ version is < 4.5, add related symbols
	sed -e 's|^ _| (lt-g++4.5)_|g' debian/libpyside${LIBPYSIDE_SOVERSION}.symbols.g++-lt-4.5 >> debian/libpyside-py3-${LIBPYSIDE_SOVERSION}.symbols-py$*
endif


#### STRIP ####
override_dh_strip:
ifdef BUILD_DEBUG
	for p in `dh_listpackages -a -Npython-pyside-dbg -Npython3-pyside-dbg`; do \
		if echo $$p | egrep -s '^python-pyside\.' > /dev/null; \
		then \
			dh_strip -p$$p --dbg-package=python-pyside-dbg --exclude=-dbg.so --exclude=_d.so ; \
		fi; \
		if echo $$p | egrep -s '^python3-pyside\.' > /dev/null; \
		then \
			dh_strip -p$$p --dbg-package=python3-pyside-dbg --exclude=-dbg.so --exclude=dm.so ; \
		fi; \
	done
	dh_strip -plibpyside-py3-$(LIBPYSIDE_SOVERSION) --dbg-package=python3-pyside-dbg
	dh_strip -plibpyside$(LIBPYSIDE_SOVERSION) --dbg-package=python-pyside-dbg
else
	dh_strip
endif
	
#### TEST ####
override_dh_auto_test: test_2 test_3

test_2: $(TEST2_TARGETS)
	@echo "Python2 versions tested."

test_3: $(TEST3_TARGETS)
	@echo "Python3 versions tested."

override_dh_auto_test_dbg-%:
	- dh_auto_test -a --builddirectory=build-$*-dbg

override_dh_auto_test-%:
	- dh_auto_test -a --builddirectory=build-$*

#### usr/share/doc/* symlinks ####
override_dh_link:
	# Replace doc directories by symlinks
	for p in `dh_listpackages -a -Nlibpyside${LIBPYSIDE_SOVERSION}`; do \
		if echo $$p | egrep -s '^python-pyside\.' > /dev/null; \
		then \
			rm -Rf debian/$$p/usr/share/doc/$$p; \
			ln -sf libpyside${LIBPYSIDE_SOVERSION} debian/$$p/usr/share/doc/$$p ; \
		fi; \
		if echo $$p | egrep -s '^python3-pyside\.' > /dev/null; \
		then \
			rm -Rf debian/$$p/usr/share/doc/$$p; \
			ln -sf libpyside-py3-${LIBPYSIDE_SOVERSION} debian/$$p/usr/share/doc/$$p ; \
		fi; \
	done
	dh_link

#### CLEAN ####
override_dh_auto_clean:
	rm -rf build-*
	rm -f setup/*.pyc
	rm -rf setup/pyside_package/
	cd tests; rm -Rf *.pyc */*.pyc */*/*.pyc */*/*/*.pyc
	- rm -rf debian/tmp-dbg
	rm -f debian/libpyside${LIBPYSIDE_SOVERSION}.symbols
	rm -f debian/libpyside${LIBPYSIDE_SOVERSION}.symbols-py*
	rm -f debian/libpyside-py3-${LIBPYSIDE_SOVERSION}.symbols
	rm -f debian/libpyside-py3-${LIBPYSIDE_SOVERSION}.symbols-py*

#### DEBIAN INSTALL ####

override_dh_install: override_dh_install_3

override_dh_install_1:
ifneq (,$(filter libpyside$(LIBPYSIDE_SOVERSION), $(shell dh_listpackages)))
	# Setup the default version symbolic links
	ln -sf libpyside-python${DEFAULT_PY}.so.${LIBPYSIDE_SOVERSION} \
		debian/tmp/usr/lib/${DEB_HOST_MULTIARCH}/libpyside${LIBPYSIDE_SOVERSION}.so
	ln -sf libpyside${LIBPYSIDE_SOVERSION}.so debian/tmp/usr/lib/${DEB_HOST_MULTIARCH}/libpyside.so
endif

override_dh_install_2: override_dh_install_1
	# Do the legacy install for the rest
ifdef BUILD_DEBUG
	dh_install -ppython-pyside-dbg --sourcedir=debian/tmp-dbg
	dh_install -ppython3-pyside-dbg --sourcedir=debian/tmp-dbg
endif
	dh_install -a -i --remaining-packages --list-missing

override_dh_install_3: override_dh_install_2
ifneq (,$(filter libpyside-dev, $(shell dh_listpackages)))
	# Make sure the pkg-config is the default's
	install -D -m 644 build-${DEFAULT_PY}/libpyside/pyside.pc \
			debian/libpyside-dev/usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig/pyside.pc
endif

override_dh_gencontrol:
	debian/dh_pycmakedeps --modules-root=PySide/
	dh_gencontrol

override_dh_builddeb:
ifdef BUILD_DEBUG
	# XZ-compress the huge python-pyside-dbg.
	dh_builddeb -ppython-pyside-dbg -ppython3-pyside-dbg -- -Zxz
endif
	dh_builddeb --remaining-packages
